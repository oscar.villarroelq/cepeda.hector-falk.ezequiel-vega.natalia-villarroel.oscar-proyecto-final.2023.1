#ifndef PARAMETROS_H
#define PARAMETROS_H

#include <QObject>

class Parametros: public QObject
{
    Q_OBJECT
    public:
        Parametros( QObject *parent = nullptr);
        double getTolerancia();
        double getPaso();
        bool getUnidad();
    public slots:
        void setTolerancia(double tol);
        void setPaso(double paso);
        void setUnidad(bool isCm);
    private:
        double tolerancia;
        double paso;
        bool isCm; //true si es cm, false si es pulgada
};

#endif // PARAMETROS_H
