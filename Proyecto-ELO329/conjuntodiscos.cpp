#include "conjuntodiscos.h"
#include <iostream>

ConjuntoDiscos::ConjuntoDiscos()
{
}

vector<int> ConjuntoDiscos::getConjunto(){
    return discos;
}
size_t ConjuntoDiscos::getSize(){
    return discos.size();
}

void ConjuntoDiscos::setConjunto(){}

void ConjuntoDiscos::addDisco(int disco){
    discos.push_back(disco);
}

bool ConjuntoDiscos::deleteDisco(int disco){
    int positiondelete;
    bool existedisco = false;
    for(int i = 0; i < (int) discos.size(); i++){
        if(discos[i] == disco){
            positiondelete = i;
            existedisco = true;
            break;
        }
    }
    if(existedisco){
        discos.erase(discos.begin()+positiondelete);
        //IMPRIME MENSAJE DE QUE SE ELIMINO EL DISCO DEL SET
        return true; //tmb se puede imprimir un mensaje directo
    }
    else{
        //IMPRIME MENSAJE DE QUE NO EXISTE EL DISCO EN EL SET
        return false;
    }
}

/*void setCuadroTexto(QPlainTextEdit* _cuadroSetDispo){
    cua
}*/

/*ojala que aca se printee la info*/
/*void ConjuntoDiscos::printDiscos(){
    cuadroSetDispo->clear();
    for(int i = 0; i < (int)discos.size(); i++){
        cuadroSetDispo->insertPlainText(QString::number(discos[i]));
        cuadroSetDispo->insertPlainText("\n");
        cout<<discos[i]<<endl;

    }
}*/


ConjuntoDiscos::~ConjuntoDiscos(){
    //delete cuadroSetDispo;
}
