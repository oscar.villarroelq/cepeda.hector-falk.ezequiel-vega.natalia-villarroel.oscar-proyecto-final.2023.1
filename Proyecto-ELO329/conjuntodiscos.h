#ifndef CONJUNTODISCOS_H
#define CONJUNTODISCOS_H

#include <vector>
#include <cmath>
//#include <QPlainTextEdit>

using namespace std;
class ConjuntoDiscos
{
public:
    ConjuntoDiscos();
    vector<int> getConjunto();
    void setConjunto();
    void addDisco(int disco);
    bool deleteDisco(int disco);
    //void printDiscos();
    //void setCuadroTexto(QPlainTextEdit* _cuadroSetDispo);
    size_t getSize();
    ~ConjuntoDiscos();

private:
    vector<int> discos;
};

#endif // CONJUNTODISCOS_H
