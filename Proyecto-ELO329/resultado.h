#ifndef RESULTADO_H
#define RESULTADO_H

#include <vector>
#include "conjuntodiscos.h"

class Resultado
{
public:
    Resultado();
    double getError();
    vector<int> getDiscos();
    void setDiscos(vector<int> _discos);
    void setError(double _error);
private:
    double error;
    //ConjuntoDiscos discos;
    vector<int> discos;
};

#endif // RESULTADO_H
