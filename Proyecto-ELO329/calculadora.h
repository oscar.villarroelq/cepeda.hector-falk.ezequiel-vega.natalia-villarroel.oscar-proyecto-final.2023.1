#ifndef CALCULADORA_H
#define CALCULADORA_H

#include <QObject>
#include <QPlainTextEdit>

#include "resultado.h"
#include "parametros.h"
#include "conjuntodiscos.h"

class Calculadora: public QObject
{
    Q_OBJECT
    public:
        Calculadora(Parametros* param, ConjuntoDiscos* set,QPlainTextEdit* cuadroResultado, QObject *parent = nullptr);
        void calcular(); //que se llame al presionar calcular. Debería retornar Resultado
        void configurarParametros(); //que se llame en metodo calcular
        void setResultado(Resultado _result);
        void ordenarVector(vector<Resultado> &_res);
        vector<Resultado> eliminarRepetidos(vector<Resultado> &resultados);

    //public slots:

    private:
        Parametros* param;
        ConjuntoDiscos* set; //descomentar cuando exista la clase ConjuntoDiscos
        QPlainTextEdit* cuadroResultado;
};

#endif // CALCULADORA_H
