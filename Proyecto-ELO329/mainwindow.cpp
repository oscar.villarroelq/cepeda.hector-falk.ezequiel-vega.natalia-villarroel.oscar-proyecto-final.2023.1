#include <QImage>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)

    /*void MainWindow::addConjuntoDiscos(){

    }*/


{
    ui->setupUi(this);
    //Crear objeto parametros
    param = new Parametros();
    ui->resultado1->setReadOnly(true);
    //botones para cambiar de página
    QPushButton * botonCalcular,* botonComoFunciona, * botonEditor,
        * botonVolverEditor, * botonVolverFunc, * botonVolverResultado;
    botonCalcular = ui->botonCalcular;
    botonCalcular->connect(botonCalcular,SIGNAL(clicked()),this,SLOT(goToResultPage()));
    botonEditor = ui->botonEditor;
    botonEditor->connect(botonEditor,SIGNAL(clicked()),this,SLOT(goToEditorPage()));
    botonComoFunciona = ui->botonComoFunciona;
    botonComoFunciona->connect(botonComoFunciona,SIGNAL(clicked()),this,SLOT(goToComoFuncionaPage()));
    botonVolverEditor = ui->botonVolverEditor;
    botonVolverEditor->connect(botonVolverEditor,SIGNAL(clicked()),this,SLOT(backToMainPage()));
    botonVolverFunc = ui->botonVolverFunc;
    botonVolverFunc->connect(botonVolverFunc,SIGNAL(clicked()),this,SLOT(backToMainPage()));
    botonVolverResultado = ui->botonVolverResultado;
    botonVolverResultado->connect(botonVolverResultado,SIGNAL(clicked()),this,SLOT(backToMainPage()));

    //añadir imagen desde código (tampoco funca)
    QImage myImageTORNO;
    QImage myImageUSM;
    QImage myImageELO;
    if (myImageTORNO.load(":/torno.png"))
        cout << "Carga exitosa imagen torno" << endl;
    if (myImageUSM.load(":/usm.png"))
        cout << "Carga exitosa imagen usm" << endl;
    if (myImageELO.load(":/elo.png"))
        cout << "Carga exitosa imagen elo" << endl;

    ui->torno->setPixmap(QPixmap::fromImage(myImageTORNO));
    ui->torno_2->setPixmap(QPixmap::fromImage(myImageTORNO));

    ui->usm->setPixmap(QPixmap::fromImage(myImageUSM));
    ui->elo->setPixmap(QPixmap::fromImage(myImageELO));

    ui->usm_2->setPixmap(QPixmap::fromImage(myImageUSM));
    ui->elo_2->setPixmap(QPixmap::fromImage(myImageELO));

    //crear calculadora y asociarle los parámetros
    //connect(ui->resultado,SIGNAL(windowIconChanged(QIcon(resultado))),this,SLOT(calcularSlot()));

    //cuadro numero de paso
    spinPasoInt = ui->spinPaso;
    spinPasoInt->connect(spinPasoInt,SIGNAL(editingFinished()),this,SLOT(setPasoSlot()));
    spinPasoInt->setValue(26);

    //cuadro tolerancia
    spinTolDouble = ui->spinTol;
    //spinTolDouble->connect(spinTolDouble,SIGNAL(editingFinished()),this,SLOT(setTolSlot()));
    spinTolDouble->connect(spinTolDouble,SIGNAL(valueChanged(double)),this,SLOT(setTolSlot()));
    spinTolDouble->setValue(0.001);

    //botones para determinar la unidad
    QPushButton * botonMm,* botonInches;
    botonMm = ui->mmButton;
    botonInches = ui->inchesButton;
    botonMm->connect(botonMm,SIGNAL(clicked()),this,SLOT(setMmSlot()));
    botonInches->connect(botonInches,SIGNAL(clicked()),this,SLOT(setInchesSlot()));

    QSlider *sliderTol = ui->horizontalSlider;
    sliderTol->connect(sliderTol, SIGNAL(sliderReleased()),this,SLOT(sliderSlot()));

    //añadir y quitar discos
    spinDisco = ui->selecionarDisco;

    QPushButton * botonAñadir,* botonEliminar;
    botonAñadir = ui->botonAgregar;
    botonEliminar = ui-> botonEliminar;

    botonAñadir->connect(botonAñadir,SIGNAL(clicked()),this,SLOT(addDisco()));
    botonEliminar->connect(botonEliminar,SIGNAL(clicked()),this,SLOT(deleteDisco()));


    ui->inchesButton->setStyleSheet("background-color: green;");

    ui->stackedWidget->setCurrentWidget(ui->pantallaPrincipal);

}

//-----------------------SLOTS CAMBIO DE VISTA----------------------

void MainWindow::goToResultPage(){
    ui->stackedWidget->setCurrentWidget(ui->resultado);
    //currentWidget() sirve para saber en cual página estas parado.
    std::cout << "calculando" << std::endl;
    ui->resultado1->clear();
    calc->calcular();
}
void MainWindow::goToEditorPage(){
    ui->stackedWidget->setCurrentWidget(ui->editorDiscos);

    //---------printear set--------
    ui->setDisp->clear();
    for(int i = 0; i < (int)conjunto->getConjunto().size(); i++){
        ui->setDisp->insertPlainText(QString::number(conjunto->getConjunto()[i]));
        ui->setDisp->insertPlainText("\n");
        cout<<conjunto->getConjunto()[i]<<endl;

    }
}
void MainWindow::goToComoFuncionaPage(){
    ui->stackedWidget->setCurrentWidget(ui->comoFunciona);
    //currentWidget() sirve para saber en cual página estas parado.
}
void MainWindow::backToMainPage(){
    ui->stackedWidget->setCurrentWidget(ui->pantallaPrincipal);
}

//------------listener de paso---------------
void MainWindow::setPasoSlot(){
    param->setPaso(spinPasoInt->value());
}

//------------------listener de tolerancia------------
void MainWindow::setTolSlot(){
    double value = spinTolDouble->value();
    param->setTolerancia(value);
    //std::cout << value*10000 << std::endl;
    ui->horizontalSlider->setSliderPosition((int)( value*100000));
    //ui->horizontalSlider->setValue((int) value*10000);
}

void MainWindow::sliderSlot(){
    std::cout << ui->horizontalSlider->value()/100000.0  << std::endl;
    double value = ui->horizontalSlider->value()/100000.0;
    ui->spinTol->setValue(value);
    param->setTolerancia(value);
    //std::cout << ui->spinTol->value()  << std::endl;
}

//---------------listener botones de unidad-------------
void MainWindow::setMmSlot(){
    param->setUnidad(true);
    ui->mmButton->setStyleSheet("background-color: green;");
    ui->inchesButton->setStyleSheet("background-color: lightGray;");
}
void MainWindow::setInchesSlot(){
    param->setUnidad(false);
    ui->mmButton->setStyleSheet("background-color: lightGray;");
    ui->inchesButton->setStyleSheet("background-color: green;");
}



//----------------------SLOT DE VISTA EDITAR SET--------------------
void MainWindow::addDisco(){
    conjunto->addDisco(spinDisco->value());
    //aca añadir un metodo para escribir el contenido del set en la vista
    (ui->labelAgreElim)->setText("Se agregó disco exitosamente");

        //---------printear set--------
        ui->setDisp->clear();
    for(int i = 0; i < (int)conjunto->getConjunto().size(); i++){
        ui->setDisp->insertPlainText(QString::number(conjunto->getConjunto()[i]));
        ui->setDisp->insertPlainText("\n");
        cout<<conjunto->getConjunto()[i]<<endl;

    }
}
void MainWindow::deleteDisco(){
    if(conjunto->deleteDisco(spinDisco->value())){
        (ui->labelAgreElim)->setText("Se eliminó disco exitosamente");
    }
    else{
        (ui->labelAgreElim)->setText("El disco que se quiere eliminar no existe");
    }
    //aca añadir un metodo para escribir el contenido del set en la vista

    //---------printear set--------
    ui->setDisp->clear();
    for(int i = 0; i < (int)conjunto->getConjunto().size(); i++){
        ui->setDisp->insertPlainText(QString::number(conjunto->getConjunto()[i]));
        ui->setDisp->insertPlainText("\n");
        cout<<conjunto->getConjunto()[i]<<endl;

    }
}

// agrega conjunto de discos a mainwindow
void MainWindow::setConjunto(ConjuntoDiscos *_set){
    conjunto = _set;
    //conjunto->setCuadroTexto(ui->setDisp);
}
// crea calculadora en mainwindow
void MainWindow::createCalc(){
    calc = new Calculadora(param,conjunto,ui->resultado1);
}
MainWindow::~MainWindow()
{
    delete ui;
    delete param;
    delete spinPasoInt;
    delete calc;
    delete conjunto;
    delete spinTolDouble;
}
