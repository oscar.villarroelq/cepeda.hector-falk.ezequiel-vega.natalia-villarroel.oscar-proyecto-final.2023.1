QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    calculadora.cpp \
    conjuntodiscos.cpp \
    main.cpp \
    mainwindow.cpp \
    parametros.cpp \
    resultado.cpp

HEADERS += \
    calculadora.h \
    conjuntodiscos.h \
    mainwindow.h \
    parametros.h \
    resultado.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images/aguila.qrc \
    images/calculo.qrc \
    images/elo.qrc \
    images/engranaje.qrc \
    images/torno.qrc \
    images/usm.qrc
