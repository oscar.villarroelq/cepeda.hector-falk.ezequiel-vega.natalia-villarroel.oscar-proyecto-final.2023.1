#include <iostream>
#include <fstream>

#include "parametros.h"

using namespace std;

Parametros::Parametros(QObject *parent): QObject(parent)
{
    // parámetros por default
    tolerancia = 0.001;
    paso = 26;
    isCm = false;
}

void Parametros::setTolerancia(double tol)
{
    tolerancia = tol;
}

void Parametros::setPaso(double paso)
{
    this->paso=paso;
    cout << "se setea paso" << endl;
    cout << paso << endl;
}

void Parametros::setUnidad(bool isCm)
{
    this->isCm = isCm;
}

double Parametros::getTolerancia()
{
    return tolerancia;
}

double Parametros::getPaso()
{
    return paso;
}


bool Parametros::getUnidad()
{
    return isCm;
}
