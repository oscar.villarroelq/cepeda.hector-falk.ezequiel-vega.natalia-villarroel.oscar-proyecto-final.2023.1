#include <QApplication>
#include <iostream>
#include <fstream>

#include "mainwindow.h"
#include "conjuntodiscos.h"

using namespace std;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    ifstream fin;
    if (argc != 2) {
        cout << "Usage: "<<argv[0]<<" <configuration_file>" << endl;
        return -1;
    }
    fin.open(argv[1]);
    if (fin.fail()){
        cout << "Could not read file" << endl;
        return -2;
    }
    cout << "Argument:" << argv[1] << endl;
    ConjuntoDiscos* discos = new ConjuntoDiscos();

    /*string linea;
    while (getline(fin, linea)) {
        // Lo vamos imprimiendo
        cout << linea << endl;
        discos->addDisco(linea);
    }*/

    int cre;
    while (fin >> cre){
        //cout << cre << endl;
        discos->addDisco(cre);
    }

    /*vector<int> setiitio =discos->getConjunto();
    for(int i = 0; i < setiitio.size(); i++){
        cout << setiitio[i] << endl;
    }*/

    //crear calculadora acá o en mainwindow
    w.setConjunto(discos);
    w.createCalc();

    w.show();
    //si hay algo raro con los set elimiar resta linea
    //había algo raroooo
    //delete discos;
    return a.exec();
}
