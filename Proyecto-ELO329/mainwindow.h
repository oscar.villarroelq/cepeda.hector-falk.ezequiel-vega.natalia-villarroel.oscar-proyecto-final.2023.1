#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSpinBox>
#include <iostream>

#include "calculadora.h"
#include "parametros.h"
#include "conjuntodiscos.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void setConjunto(ConjuntoDiscos *_set);
    void createCalc();
    ~MainWindow();
    //void addConjuntoDiscos();
private slots:
    void goToResultPage();
    void goToEditorPage();
    void goToComoFuncionaPage();
    void backToMainPage();
    void setPasoSlot();
    void setTolSlot();
    void setMmSlot();
    void setInchesSlot();
    void sliderSlot();
    void addDisco();
    void deleteDisco();
private:
    Ui::MainWindow *ui;
    Calculadora* calc;
    Parametros* param;
    ConjuntoDiscos *conjunto;
    QDoubleSpinBox* spinPasoInt;
    QDoubleSpinBox* spinTolDouble;
    QSpinBox* spinDisco;
    //ResultadoView* resultado;
};
#endif // MAINWINDOW_H
